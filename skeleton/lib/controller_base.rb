require 'active_support'
require 'active_support/core_ext'
require 'erb'
require_relative './session'
require 'active_support/inflector'
require 'byebug'

class ControllerBase
  attr_reader :req, :res, :params

  # Setup the controller
  def initialize(req, res, options = {})
    @req = req
    @res = res
  end

  # Helper method to alias @already_built_response
  def already_built_response?
    @already_built_response
  end

  # Set the response status code and header
  def redirect_to(url)
    raise "double render" if self.already_built_response?
    res["Location"] = url
    res.status = 302
    @already_built_response = true
    @session.store_session(res)
  end

  # Populate the response with content.
  # Set the response's content type to the given type.
  # Raise an error if the developer tries to double render.
  def render_content(content, content_type)
    raise "double render" if self.already_built_response?
    res.write(content)
    @already_built_response = true
    res['Content-Type'] = content_type
    @session.store_session(res)
  end

  # use ERB and binding to evaluate templates
  # pass the rendered html to render_content
  def render(template_name)
    puts "---------#{self.req.path}+++++----------"
    file = "views/#{self.class.to_s.underscore}/#{template_name.to_s}.html.erb"
    view_content = File.read(file)
    html = ERB.new(view_content).result(binding)
    @already_built_response = true
    render_content(html, 'text/html')
  end

  # method exposing a `Session` object
  def session
    @session ||= Session.new(req)
  end

  # use this with the router to call action_name (:index, :show, :create...)
  def invoke_action(name)
    send(name)
    render(name) unless already_built_response?
  end
end
