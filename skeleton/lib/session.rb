require 'json'

class Session
  # find the cookie for this app
  # deserialize the cookie into a hash
  attr_reader :cookie
  def initialize(req)
    cookie_val = req.cookies['_rails_lite_app']
    @cookie = cookie_val ? JSON.parse(cookie_val) : {}
  end

  def [](key)
    self.cookie[key]
  end

  def []=(key, val)
    self.cookie[key] = val
  end

  # serialize the hash into json and save in a cookie
  # add to the responses cookies
  def store_session(res)
    value = @cookie.to_json
    cookie = { path: "/", value: value }
    res.set_cookie('_rails_lite_app', cookie)
  end
end
